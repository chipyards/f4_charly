
/* Buffer used for transmission */
extern char txbuf2[96];
/* Buffer used for reception */
extern volatile char rxchar2;

void uart2_init( unsigned int baudrate );
void gpio_uart2_init( void );
// void uart2_blocking_tx( const char * tbuf );
void uart2_printf( const char *fmt, ... );
