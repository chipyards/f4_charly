/* MENU SYSTEM, repondant a des commandes d'un caractere
Il y a 2 types de menu :
	- menu ordinaire : une liste d'items, chacun choisi par une touche
	- editeur de param : interprete quelques touches specialisees pour ajuster un param de l'appli
Param des menus et items :
	- chaque item a un param constant
	- lorsqu'un menu (ou editeur) est invoque au travers d'un item, il acquiert ce param
	- un item peut heriter du param du menu courant (si son param est HERIT_PARAM)
	- exemple d'application : les params 'X', 'Y', 'Z' permettent d'avoir des menus et editeurs generiques pour les 3 axes
Touche speciales :
	TAB affiche le menu courant
	Esc depile le menu si possible
	= si editeur de param, affiche la valeur courante
	<, >, 0, etc pour editeur de param (facultatives)
Touches non traitees par menu ou editeur de param : sont deferees a la main loop de l'application
*/

#define MENU_NEST 4	// nombre max de niveaux d'imbrication

#define IS_EDIT_FUNC(q) (q>25)
#define HERIT_PARAM (0xF0000000)

// item dans un menu
typedef struct {
	const char * label;
	const void * adr;
	int param;	// HERIT_PARAM <==> parametre herite du menu parent
	char key;
	char type;	// 0=simple message, 1=submenu, 2=fonction
	} menuitem_t;

// menu
typedef struct {
	const char * title;
	int qitems;	// ou adresse de fonction d'edition, selon IS_EDIT_FUNC()
	menuitem_t items[];
	} menu_t;

// pile des menus en cours (singleton)
typedef struct {
	menu_t * menus[MENU_NEST];
	int sp;			// index menu en cours
	int params[MENU_NEST];	// parametre du menu en cours, acquis du menuitem
	} menustack_t;

// affiche le menu courant
void menu_show();

// si key est TAB, affiche le menu courant
// si la key est Esc, depile le menu si possible 
// sinon execute la commande si trouvee dans le menu courant et rend 0,
// sinon rend la key
int menu_exec( char key );

