
// params pour move
extern deviceParams_t devicePrm[];

// editeurs standard
// hard
int edit_ustep( char key , int param );
int edit_tval( char key, int param );
int edit_ocd_th( char key, int param );
int edit_toff_fast( char key, int param );
int edit_fall_step( char key, int param );
int edit_ton_min( char key, int param );
int edit_toff_min( char key, int param );
int edit_toff( char key, int param );
int edit_slew_rate( char key, int param );

// soft
int edit_accel( char key, int param );
int edit_decel( char key, int param );
int edit_fmin( char key, int param );
int edit_fmax( char key, int param );
int edit_abs_pos( char key, int param );
int edit_rel_move( char key, int param );

// run
void cont_run_fwd( int param );
void cont_run_back( int param );
void cont_run_stop( int param );

// reports
void report_L6474_status( int device_id, int clear_flags, int label_flag );
void hard_show_all( int param );
void soft_show_all( int param );

// utilitaires
void soft_param_init( int device_id );
void show_help();
void my_defaults( int param );
