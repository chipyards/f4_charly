// fonctions d'edition de parametres
// chaque fonction doit repondre au moins a la key '=' pour afficher la valeur courante
// et doit retourner 0, ou la key si elle est ignoree

#include "uart2.h"
#include <stdio.h>	// pour snprintf
#include "l6474.h"
#include "param.h"

///
/// params du L6474
///

// torque TVAL
int edit_tval( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -3;	break;
	case ')' : inc = 3;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_TVAL ) & 0x7F;
if	( inc )
	{
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 127 ) val = 127;
	L6474_CmdSetParam( device_id, L6474_TVAL, val );
	val = L6474_CmdGetParam( device_id, L6474_TVAL ) & 0x7F;
	}
uart2_printf("T_VAL nom %u mA\n", ( ( val * 31250 ) + 31250 + 500 ) / 1000 );
return 0;
}

// Overcurrent limit
int edit_ocd_th( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_OCD_TH ) & 0xF;
if	( inc )
	{
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 15 ) val = 15;
	L6474_CmdSetParam( device_id, L6474_OCD_TH, val );
	val = L6474_CmdGetParam( device_id, L6474_OCD_TH ) & 0xF;
	}
uart2_printf("OCD limit %u mA\n", ( val + 1 ) * 375 );
return 0;
}

// microstep
static int get_ustep( int device_id )
{
int val = L6474_CmdGetParam( device_id, L6474_STEP_MODE ) & 7;
if	( val & 4 ) val = 4;
return val;
}

int edit_ustep( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	default : return key;
	}
val = get_ustep( device_id );
if	( inc )
	{
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 4 ) val = 4;
	L6474_CmdSetParam( device_id, L6474_STEP_MODE, 0x88 | val );
	val = get_ustep( device_id );
	}
uart2_printf("Microstep 1/%d\n", 1 << val );
return 0;
}

// auto off system pour editer le timing a chaud (return : not HiZ)
static int force_off( int device_id )
{
int status = L6474_CmdGetParam( device_id, L6474_STATUS );
if	( ( status & 1 ) == 0 )
	{			// not HiZ, then disable
	L6474_CmdDisable( device_id );
	return 1;
	}
return 0;
}

// Toff Fast (hi half of T_FAST reg)
int edit_toff_fast( char key, int param )
{
int val, val_hi, val_lo, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_T_FAST );
val_hi = val >> 4;
if	( inc )
	{
	int forcedoff = force_off( device_id );
	val_lo = val & 0xF;
	val_hi += inc;
	if	( val_hi < 0 ) val_hi = 0;
	if	( val_hi > 15 ) val_hi = 15;
	val = val_lo | ( val_hi << 4 );
	L6474_CmdSetParam( device_id, L6474_T_FAST, val );
	if ( forcedoff ) L6474_CmdEnable( device_id );
	val_hi = L6474_CmdGetParam( device_id, L6474_T_FAST ) >> 4;
	}
uart2_printf("Toff Fast %u us\n", ( val_hi + 1 ) * 2 );
return 0;
}

// Fall Step (lo half of T_FAST reg)
int edit_fall_step( char key, int param )
{
int val, val_hi, val_lo, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_T_FAST );
val_lo = val & 0xF;
if	( inc )
	{
	int forcedoff = force_off( device_id );
	val_hi = val >> 4;
	val_lo += inc;
	if	( val_lo < 0 ) val_lo = 0;
	if	( val_lo > 15 ) val_lo = 15;
	val = val_lo | ( val_hi << 4 );
	L6474_CmdSetParam( device_id, L6474_T_FAST, val );
	if ( forcedoff ) L6474_CmdEnable( device_id );
	val_lo = L6474_CmdGetParam( device_id, L6474_T_FAST ) & 0xF;
	}
uart2_printf("Fall Step %u us\n", ( val_lo + 1 ) * 2 );
return 0;
}

// Ton Min
int edit_ton_min( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -4;	break;
	case ')' : inc = 4;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_TON_MIN ) & 0x7F;
if	( inc )
	{
	int forcedoff = force_off( device_id );
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 127 ) val = 127;
	L6474_CmdSetParam( device_id, L6474_TON_MIN, val );
	if ( forcedoff ) L6474_CmdEnable( device_id );
	val = L6474_CmdGetParam( device_id, L6474_TON_MIN ) & 0x7F;
	}
val += 1;
uart2_printf("Ton  min  %u.%u us\n", val / 2, ( val & 1 ) * 5  );
return 0;
}

// Toff Min (la valeur appliquee sera le max de Toff et Toff Min ??)
int edit_toff_min( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -4;	break;
	case ')' : inc = 4;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_TOFF_MIN ) & 0x7F;
if	( inc )
	{
	int forcedoff = force_off( device_id );
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 127 ) val = 127;
	L6474_CmdSetParam( device_id, L6474_TOFF_MIN, val );
	if ( forcedoff ) L6474_CmdEnable( device_id );
	val = L6474_CmdGetParam( device_id, L6474_TOFF_MIN ) & 0x7F;
	}
val += 1;
uart2_printf("Toff min  %u.%u us\n", val / 2, ( val & 1 ) * 5  );
return 0;
}

// Toff (la valeur appliquee sera le max de Toff et Toff Min ??)
int edit_toff( char key, int param )
{
int val, toff, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -4;	break;
	case ')' : inc = 4;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_CONFIG );
toff = ( val >> 10 ) & 0x1F;
if	( inc )
	{
	int forcedoff = force_off( device_id );
	toff += inc;
	if	( toff < 1 ) toff = 1;
	if	( toff > 31 ) toff = 31;
	val &= (~0x7C00);
	val |= ( toff << 10 );
	L6474_CmdSetParam( device_id, L6474_CONFIG, val );
	if ( forcedoff ) L6474_CmdEnable( device_id );
	toff = ( L6474_CmdGetParam( device_id, L6474_CONFIG ) >> 10 ) & 0x1F;
	}
uart2_printf("Toff      %u us\n", ( toff * 4 )  );
return 0;
}

// slew rate
int edit_slew_rate( char key, int param )
{
static const short slew_val[4] = { 320, 75, 110, 260 };
int val, slew, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_CONFIG );
slew = ( val >> 8 ) & 0x3;
if	( inc )
	{
	slew += inc;
	if	( slew < 0 ) slew = 3;
	if	( slew > 3 ) slew = 0;
	val &= (~0x0300);
	val |= ( slew << 8 );
	L6474_CmdSetParam( device_id, L6474_CONFIG, val );
	slew = ( L6474_CmdGetParam( device_id, L6474_CONFIG ) >> 8 ) & 0x3;
	}
uart2_printf("Slew Rate %u V/us\n", slew_val[slew] );
return 0;
}

// lecture status avec RAZ des flags optionnelle
void report_L6474_status( int device_id, int clear_flags, int label_flag )
{
int status;
if	( clear_flags )
	status = L6474_CmdGetStatus( device_id );
else	status = L6474_CmdGetParam( device_id, L6474_STATUS );
int OCD		= ( ( status >> 12 ) & 1 ) ^ 1;
int TH_SD	= ( ( status >> 11 ) & 1 ) ^ 1;
int TH_WRN	= ( ( status >> 10 ) & 1 ) ^ 1;
int UVLO	= ( ( status >>  9 ) & 1 ) ^ 1;
int WRONG_CMD	=   ( status >>  8 ) & 1;
int NOTPERF	=   ( status >>  7 ) & 1;
int HiZ		= status & 1;
if	( label_flag )
	uart2_printf("    OCD     TH_SD   TH_WRN  UVLO    WRONG   NOTPERF HiZ\n");
uart2_printf("    %d       %d       %d       %d       %d       %d       %d\n",
	OCD, TH_SD, TH_WRN, UVLO, WRONG_CMD, NOTPERF, HiZ );
}

// recap de tous les parametres
void hard_show_all( int param )
{
uart2_printf("--- axe %c ---\n", param );
edit_ustep( '=', param );
edit_tval( '=', param );
edit_ocd_th( '=', param );
edit_toff_fast( '=', param );
edit_fall_step( '=', param );
edit_ton_min( '=', param );
edit_toff_min( '=', param );
edit_toff( '=', param );
edit_slew_rate( '=', param );
int device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return;
report_L6474_status( device_id, 0, 1 );
}

// defaults values, simpler than through l6474_target_config
void my_defaults( int param )
{
int device_id = param - 'X';
// Toff Min (la valeur appliquee sera le max de Toff et Toff Min ??)
int toffmin = (int)( 22.5 * 2 ) - 1;
L6474_CmdSetParam( device_id, L6474_TOFF_MIN, toffmin & 0x7F );
edit_toff_min( '=', param );	// just for print

// Ton min
int tonmin = (int)( 2.5 * 2 ) - 1;
L6474_CmdSetParam( device_id, L6474_TON_MIN, tonmin & 0x7F );
edit_ton_min( '=', param );	// just for print

// Toff Fast (hi half of T_FAST reg)
int tfast = ( 24 / 2 ) - 1;
// Fall Step (lo half of T_FAST reg)
int fstep = ( 12 / 2 ) - 1;
L6474_CmdSetParam( device_id, L6474_T_FAST, ( fstep & 0xF ) | ( ( tfast & 0xF ) << 4 ) );
edit_toff_fast( '=', param );	// just for print
edit_fall_step( '=', param );

// slew rate
int slew = 3;
int val = L6474_CmdGetParam( device_id, L6474_CONFIG );
val &= (~0x0300);
val |= ( ( slew & 3 ) << 8 );
L6474_CmdSetParam( device_id, L6474_CONFIG, val );
edit_slew_rate( '=', param );	// just for print
}


///
/// params du move
///
// cette fonction remplace L6474_SetDeviceParamsToPredefinedValues()
void soft_param_init( int device_id )
{
devicePrm[device_id].acceleration = 2000;
devicePrm[device_id].deceleration = 4000;
devicePrm[device_id].maxSpeed = 1000;
devicePrm[device_id].minSpeed = 100;
devicePrm[device_id].stopMode = HOLD_MODE;	// ajoute ici, etait avec registers

devicePrm[device_id].accu = 0;
devicePrm[device_id].currentPosition = 0;
devicePrm[device_id].endAccPos = 0;
devicePrm[device_id].relativePos = 0;
devicePrm[device_id].startDecPos = 0;
devicePrm[device_id].stepsToTake = 0;
devicePrm[device_id].speed = 0;
devicePrm[device_id].commandExecuted = NO_CMD;
devicePrm[device_id].direction = FORWARD;
devicePrm[device_id].motionState = INACTIVE;
}

void show_help()
{
uart2_printf("=== config %d axes ===\n", L6474_GetNbDevices() );
uart2_printf("Commandes globales:\n");
uart2_printf(" TAB : affiche le menu courant\n");
uart2_printf(" Esc : depile le menu si possible\n");
uart2_printf(" sp : stop all, position courante\n");
uart2_printf(" r : L6474_Reset (all)\n");
uart2_printf(" R : L6474_ReleaseReset (all)\n");
uart2_printf(" * : L6474_CmdDisable = HiZ (all)\n");
uart2_printf(" E : L6474_CmdEnable (all)\n");
uart2_printf(" s : report_L6474_status\n");
uart2_printf(" S : report_L6474_status & clear\n");
uart2_printf("Commandes d'edition param:\n");
uart2_printf(" = : valeur courante du param\n");
uart2_printf(" <> : decrementer, incrementer\n");
uart2_printf(" () : -+10, [] -+ 100, {} -+ 1000\n");
uart2_printf("ATTENTION : param avec * editable seulement en HiZ\n");
uart2_printf("Commandes debug:\n");
uart2_printf(" H : system clock\n");
uart2_printf(" I : report_interrupts\n");
uart2_printf(" g : get config\n");
uart2_printf(" t : get T_VAL\n");
uart2_printf(" z : get step mode\n");
}

// acceleration en Hz/s
int edit_accel( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	case '[' : inc = -100;	break;
	case ']' : inc = 100;	break;
	case '{' : inc = -1000;	break;
	case '}' : inc = 1000;	break;
	default : return key;
	}
val = L6474_GetAcceleration( device_id ) & 0x7FFF;
if	( inc )
	{
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 0x7FFF ) val = 0x7FFF;
	L6474_SetAcceleration( device_id, val );
	val = L6474_GetAcceleration( device_id ) & 0x7FFF;
	}
uart2_printf("Accel     %u Hz/s\n", val  );
return 0;
}

// deceleration en Hz/s
int edit_decel( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	case '[' : inc = -100;	break;
	case ']' : inc = 100;	break;
	case '{' : inc = -1000;	break;
	case '}' : inc = 1000;	break;
	default : return key;
	}
val = L6474_GetDeceleration( device_id ) & 0x7FFF;
if	( inc )
	{
	val += inc;
	if	( val < 0 ) val = 0;
	if	( val > 0x7FFF ) val = 0x7FFF;
	L6474_SetDeceleration( device_id, val );
	val = L6474_GetDeceleration( device_id ) & 0x7FFF;
	}
uart2_printf("Decel     %u Hz/s\n", val  );
return 0;
}

// frequ min en Hz
int edit_fmin( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	case '[' : inc = -100;	break;
	case ']' : inc = 100;	break;
	case '{' : inc = -1000;	break;
	case '}' : inc = 1000;	break;
	default : return key;
	}
val = L6474_GetMinSpeed( device_id ) & 0x7FFF;
if	( inc )
	{
	val += inc;
	if	( val < 1 ) val = 1;
	if	( val > 0x7FFF ) val = 0x7FFF;
	L6474_SetMinSpeed( device_id, val );
	val = L6474_GetMinSpeed( device_id ) & 0x7FFF;
	}
uart2_printf("Min Speed %u Hz (%u FSps)\n", val, val / ( 1 << get_ustep( device_id ) ) );
return 0;
}

// frequ max en Hz
int edit_fmax( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	case '[' : inc = -100;	break;
	case ']' : inc = 100;	break;
	case '{' : inc = -1000;	break;
	case '}' : inc = 1000;	break;
	default : return key;
	}
val = L6474_GetMaxSpeed( device_id ) & 0x7FFF;
if	( inc )
	{
	val += inc;
	if	( val < 1 ) val = 1;
	if	( val > 0x7FFF ) val = 0x7FFF;
	L6474_SetMaxSpeed( device_id, val );
	val = L6474_GetMaxSpeed( device_id ) & 0x7FFF;
	}
uart2_printf("Max Speed %u Hz (%u FSps)\n", val, val / ( 1 << get_ustep( device_id ) ) );
return 0;
}

// electrical pos
static int get_el_pos( int device_id )
{
int val = L6474_CmdGetParam( device_id, L6474_EL_POS ) & 0x1FF;
return val >> 3;	// en 1/16
}

// abs pos
int edit_abs_pos( char key, int param )
{
int val, inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
switch	( key )
	{
	case '0' :
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	default : return key;
	}
val = L6474_CmdGetParam( device_id, L6474_ABS_POS ) & 0x3FFFFF;
if	( val & 0x200000 ) val |= (~0x3FFFFF);	// sign extension
if	( ( inc ) || ( key == '0' ) )
	{
	if	( key == '0' )
		val = 0;
	else	val += inc;
	L6474_CmdSetParam( device_id, L6474_ABS_POS, val & 0x3FFFFF );
	val = L6474_CmdGetParam( device_id, L6474_ABS_POS ) & 0x3FFFFF;
	if	( val & 0x200000 ) val |= (~0x3FFFFF);	// sign extension
	}
int ustep = get_ustep( device_id );
int el_pos = get_el_pos( device_id ) >> ( 4 - ustep );
uart2_printf("Abs Pos   %d (%d FS) (el_pos %u/%u)\n", val, val / ( 1 << ustep ), el_pos, (1 << ustep) );
return 0;
}

// relative move
int edit_rel_move( char key, int param )
{
static int relmove[] = { 1, 1, 1 };
int inc = 0, device_id = param - 'X';
if	( ( device_id < 0 ) || ( device_id > 2 ) )
	return 0;
if	( L6474_GetDeviceState( device_id ) != INACTIVE )
	{
	uart2_printf("motor \"not inactive\"\n");
	return 0;
	}
switch	( key )
	{
	case 'F' :
	case 'B' :
	case '1' :
	case '=' : inc = 0;	break;
	case '<' : inc = -1;	break;
	case '>' : inc = 1;	break;
	case '(' : inc = -10;	break;
	case ')' : inc = 10;	break;
	case '[' : inc = -100;	break;
	case ']' : inc = 100;	break;
	case '{' : inc = -1000;	break;
	case '}' : inc = 1000;	break;
	default : return key;
	}
if	( inc )
	{
	relmove[device_id] += inc;
	if	( relmove[device_id] < 1 )
		relmove[device_id] = 1;
	}
else if	( key == '1' )
	relmove[device_id] = 1;
else if	( key == 'F' )	// abs pos augmente
	{
	L6474_Move( device_id, 1, relmove[device_id] );
	uart2_printf("moving %d\n", relmove[device_id] );
	return 0;
	}
else if	( key == 'B' )	// abs pos diminue
	{
	L6474_Move( device_id, 0, relmove[device_id] );
	uart2_printf("moving %d\n", -relmove[device_id] );
	return 0;
	}
int ustep = get_ustep( device_id );
uart2_printf("Rel Move  %u (%u FS)\n", relmove[device_id], relmove[device_id] / ( 1 << ustep ) );
return 0;
}

// recap de tous les parametres
void soft_show_all( int param )
{
uart2_printf("--- axe %c ---\n", param );
edit_accel( '=', param );
edit_decel( '=', param );
edit_fmin( '=', param );
edit_fmax( '=', param );
edit_abs_pos( '=', param );
}

void cont_run_fwd( int param )
{
int device_id = param - 'X';
L6474_Run( device_id, 1 );
}

void cont_run_back( int param )
{
int device_id = param - 'X';
L6474_Run( device_id, 0 );
}

void cont_run_stop( int param )
{
int device_id = param - 'X';
L6474_SoftStop( device_id );
}

