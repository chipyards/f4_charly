
#include "uart2.h"
#include <stdio.h>	// pour snprintf
#include "menu.h"
#include "l6474.h"
#include "param.h"

///
/// data de l'application
///

// editeurs de param (chacun a un pointeur sur une fonction declaree dans param.h)
const menu_t ustep_medit = { "Microstep", (int)edit_ustep, {} };
const menu_t tval_medit = { "Courant nominal", (int)edit_tval, {} };
const menu_t ocd_medit = { "Courant limite", (int)edit_ocd_th, {} };
const menu_t toff_fast_medit = { "Toff Fast", (int)edit_toff_fast, {} };
const menu_t fall_step_medit = { "Fall Step", (int)edit_fall_step, {} };
const menu_t ton_min_medit = { "Ton min", (int)edit_ton_min, {} };
const menu_t toff_min_medit = { "Toff min", (int)edit_toff_min, {} };
const menu_t toff_medit = { "Toff", (int)edit_toff, {} };
const menu_t slew_rate_medit = { "Slew Rate", (int)edit_slew_rate, {} };
const menu_t accel_medit = { "Accel", (int)edit_accel, {} };
const menu_t decel_medit = { "Decel", (int)edit_decel, {} };
const menu_t fmin_medit = { "Fmin", (int)edit_fmin, {} };
const menu_t fmax_medit = { "Fmax", (int)edit_fmax, {} };
const menu_t abs_pos_medit = { "Abs Pos [0<>()]", (int)edit_abs_pos, {} };
const menu_t rel_move_medit = { "Rel Move [1FB<>()[]{}]", (int)edit_rel_move, {} };

// menus
const menu_t hard_menu = { "L6474 axe", 11, {
	{ "Recapitulation", hard_show_all, HERIT_PARAM, 'R', 2 },
	{ "Microstep *", &ustep_medit, HERIT_PARAM, 'u', 1 },
	{ "Courant nominal", &tval_medit, HERIT_PARAM, 'c', 1 },
	{ "Courant limite", &ocd_medit, HERIT_PARAM, 'O', 1 },
	{ "Toff Fast *", &toff_fast_medit, HERIT_PARAM, 'a', 1 },
	{ "Fall Step *", &fall_step_medit, HERIT_PARAM, 'A', 1 },
	{ "Ton min *", &ton_min_medit, HERIT_PARAM, 'n', 1 },
	{ "Toff min *", &toff_min_medit, HERIT_PARAM, 'f', 1 },
	{ "Toff *", &toff_medit, HERIT_PARAM, 'F', 1 },
	{ "Slew Rate", &slew_rate_medit, HERIT_PARAM, 'w', 1 },
	{ "Soft Init", my_defaults, HERIT_PARAM, 'D', 2 },
	} };

const menu_t soft_menu = { "Move axe", 10, {
	{ "Recapitulation", soft_show_all, HERIT_PARAM, 'R', 2 },
	{ "Accel", &accel_medit, HERIT_PARAM, 'a', 1 },
	{ "Decel", &decel_medit, HERIT_PARAM, 'd', 1 },
	{ "Fmin", &fmin_medit, HERIT_PARAM, 'i', 1 },
	{ "Fmax", &fmax_medit, HERIT_PARAM, 'f', 1 },
	{ "Abs Pos", &abs_pos_medit, HERIT_PARAM, 'p', 1 },
	{ "Rel Move", &rel_move_medit, HERIT_PARAM, 'M', 1 },
	{ "Run Forward", cont_run_fwd, HERIT_PARAM, 'U', 2 },
	{ "Run Back", cont_run_back, HERIT_PARAM, 'V', 2 },
	{ "Run Stop", cont_run_stop, HERIT_PARAM, ' ', 2 },
	// { "", &_medit, HERIT_PARAM, '', 1 },
	} };

const menu_t root_menu = { "Charly UI", 5, {
	{ "Params L6474 axe X", &hard_menu, 'X', 'X', 1 },
	{ "Params Moves axe X", &soft_menu, 'X', 'x', 1 },
	{ "Params L6474 axe Y", &hard_menu, 'Y', 'Y', 1 },
	{ "Params Moves axe Y", &soft_menu, 'Y', 'y', 1 },
	{ "Help", show_help, 0, 'h', 2 }
	} };

// pile des menus en cours (singleton) - attention doit respecter MENU_NEST
menustack_t mstack = { { (menu_t *)&root_menu, 0, 0, 0 }, 0, { 0, 0, 0, 0 } };

///
///	fonctions du systeme de menus
///

void menu_show()
{
menu_t * m = mstack.menus[mstack.sp];
int p = mstack.params[mstack.sp];
uart2_printf("\n=== %s", m->title );
if	( ( p > ' ' ) && ( p <= '~' ) )
	uart2_printf(" %c", p );
uart2_printf(" ===\n");
if	( IS_EDIT_FUNC( m->qitems ) )
	{
	int (* lafun)(char,int);
	lafun = (int (*)(char,int))m->qitems;
	lafun( '=', mstack.params[mstack.sp] );
	return;
	}
for	( int i = 0; i < m->qitems; i++ )
	uart2_printf(" %c: %s\n", m->items[i].key, m->items[i].label );
}

// affiche le nouveau menu, empile son adresse et son param, rend la main
static void menu_push( menu_t * m, int p )
{
if	( mstack.sp >= ( sizeof(mstack.menus) - 1 ) )
	return;
mstack.sp++;
mstack.menus[mstack.sp] = m;
mstack.params[mstack.sp] = p;
menu_show();
}

// affiche l'ancien menu, depile son adresse et son param, rend la main
static void menu_pop()
{
if	( mstack.sp <= 0 )
	return;
mstack.sp--;
menu_show();
}

int menu_exec( char key )
{
if	( key == 0x1B )		// Esc
	{ menu_pop(); return 0;	}
if	( key == 0x09 )		// Tab
	{ menu_show(); return 0; }
menu_t * m = mstack.menus[mstack.sp];
if	( IS_EDIT_FUNC( m->qitems ) )
	{
	int (* lafun)(char,int);
	lafun = (int (*)(char,int))m->qitems;
	key = lafun( key, mstack.params[mstack.sp] );
	return key;
	}
for	( int i = 0; i < m->qitems; i++ )
	{
	menuitem_t * it = &m->items[i];
	if	( key == it->key )
		{
		// traitement heritage du param
		int local_param;
		if	( it->param == HERIT_PARAM )
			local_param = mstack.params[mstack.sp];
		else	local_param = it->param;
		switch	( it->type )
			{
			case 0: 	// simple message
				uart2_printf("%s\n",  (char * )it->adr );
				break;
			case 1:		// activation sub-menu, avec acquisition du param
				menu_push( (menu_t * )it->adr, local_param );
				break;
			case 2:		// function
				{
				void (* lafun)(int);
				lafun = (void (*)(int))it->adr;
				lafun( local_param );
				} break;
			}
		return 0;
		}
	}
return key;
}
