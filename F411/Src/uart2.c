#include "stm32f4xx_hal.h"
#include "stm32f4xx_nucleo.h"

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_usart.h"

#include "uart2.h"
#include <stdio.h>
#include <stdarg.h>
// #include <string.h>	// pour strlen

// ridiculous global data
static UART_HandleTypeDef UartHandle;

/* Buffer used for transmission */
char txbuf2[96];
/* Buffer used for reception */
volatile char rxchar2 = 0;

// Interrupt Handlers
void USART2_IRQHandler(void)
{
/* Check RXNE flag value in SR register */
if	( LL_USART_IsActiveFlag_RXNE(USART2) && LL_USART_IsEnabledIT_RXNE(USART2) )
	{
	rxchar2 = LL_USART_ReceiveData8(USART2);
	}
}

/* Configure the UART peripheral using HAL services ###################*/
void uart2_init( unsigned int baudrate )
{
__HAL_RCC_USART2_CLK_ENABLE();

UartHandle.Instance        = USART2;

UartHandle.Init.BaudRate   = baudrate;
UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
UartHandle.Init.StopBits   = UART_STOPBITS_1;
UartHandle.Init.Parity     = UART_PARITY_NONE;
UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
UartHandle.Init.Mode       = UART_MODE_TX_RX;
UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

if	(HAL_UART_Init(&UartHandle) != HAL_OK)
  	while(1) {}

LL_USART_EnableIT_RXNE(USART2);
}


void gpio_uart2_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /* Enable GPIO TX/RX clock */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = GPIO_PIN_2;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_MEDIUM;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;

  HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = GPIO_PIN_3;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;

  HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );

  /*##-3- Configure the NVIC for UART ########################################*/
  HAL_NVIC_SetPriority( USART2_IRQn, 11, 1 );
  HAL_NVIC_EnableIRQ( USART2_IRQn );
}

//void uart2_blocking_tx( const char * tbuf )
//{
//HAL_UART_Transmit( &UartHandle, (unsigned char *)tbuf, strlen(tbuf), 1000 );
//}

void uart2_printf( const char *fmt, ... )
{
va_list  argptr;
va_start( argptr, fmt );
int cnt = vsnprintf( txbuf2, sizeof(txbuf2), fmt, argptr );
va_end( argptr );
if	( cnt >= sizeof(txbuf2) )
	cnt = sizeof(txbuf2) - 1;
if	( cnt > 0 )
	HAL_UART_Transmit( &UartHandle, (unsigned char *)txbuf2, cnt, 1000 );
}

