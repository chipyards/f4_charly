/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_nucleo.h"

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_pwr.h"

#include <stdio.h>	// pour snprintf

#include "uart2.h"
#include "l6474.h"
#include "param.h"
#include "menu.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

//
volatile unsigned int debug_resu = 0xDEADBEEF;

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);

/* Interrupt Handlers ---------------------------------------------------------*/

void SysTick_Handler(void)
{
HAL_IncTick();
}

/* functions */

void report_interrupts(void)
{
int i;
unsigned int p;
p = __NVIC_GetPriorityGrouping();
uart2_printf("priority grouping %d\n", p );
// special systick
i = -1;
if	(  SysTick->CTRL & SysTick_CTRL_TICKINT_Msk )
	{
	p = __NVIC_GetPriority((IRQn_Type)i);
	uart2_printf("int #%2d, pri %d\n", i, p );
	}
// tous les autres
for	( i = 0; i <= 86; ++i )	// max i : cf stm32f411xe.h
	{
	if	( __NVIC_GetEnableIRQ((IRQn_Type)i) )
		{
		p = __NVIC_GetPriority((IRQn_Type)i);
		uart2_printf("int #%2d, pri %d\n", i, p );
		}
	}
}



void serial_command_parser( int c )
{
c = menu_exec( c );
if	( c == 0 )
	return;
switch	( c )
	{
	case 'H' :
		uart2_printf( "system clock = %u Hz\n", (unsigned int)SystemCoreClock );
       		break;
       	case 'I' :
       		report_interrupts();
       		break;
       	// tests unitaires GPIO
	case 'r' :
		L6474_Reset(0);			// PA9 reset commun aux 3 devices
		break;
	case 'R' :
		L6474_ReleaseReset(0);		// PA9 reset commun aux 3 devices
		break;
       	// tests unitaires SPI			// PB6, PA5, 6, 7
	case '*' :
		L6474_CmdDisable(0);		// cmd A8
		if	( L6474_GetNbDevices() > 1 )
			L6474_CmdDisable(1);
		if	( L6474_GetNbDevices() > 2 )
			L6474_CmdDisable(2);
		break;
	case 'E' :
		L6474_CmdEnable(0);		// cmd B8
		if	( L6474_GetNbDevices() > 1 )
			L6474_CmdEnable(1);
		if	( L6474_GetNbDevices() > 2 )
			L6474_CmdEnable(2);
		break;
	// status
	case 's' :
		report_L6474_status( 0, 0, 1 );
		if	( L6474_GetNbDevices() > 1 )
			report_L6474_status( 1, 0, 0 );
		if	( L6474_GetNbDevices() > 2 )
			report_L6474_status( 2, 0, 0 );
		break;
	case 'S' :
		report_L6474_status( 0, 1, 1 );
		if	( L6474_GetNbDevices() > 1 )
			report_L6474_status( 1, 1, 0 );
		if	( L6474_GetNbDevices() > 2 )
			report_L6474_status( 2, 1, 0 );
		break;
	// params operationnels
	case 'g' :
		debug_resu = L6474_CmdGetParam( 0, L6474_CONFIG );	//
		uart2_printf("config \"2E88\" : %04x\n", debug_resu );
		break;
	case 't' :
		debug_resu = 1 + L6474_CmdGetParam( 0, L6474_TVAL );
		uart2_printf("TVAL=%u ==> %u uA\n", debug_resu, debug_resu * 31250 );
		break;
	case 'z' :
		debug_resu = L6474_CmdGetParam( 0, L6474_STEP_MODE );	//
		uart2_printf("step mode %02x => %u\n", debug_resu, debug_resu & 7 );
		break;
	// raccourcis
	case ' ' :
		L6474_HardStop( 0 );
		edit_abs_pos( '=', 'X' );	// position
		if	( L6474_GetNbDevices() > 1 )
			{
			L6474_HardStop( 1 );
			edit_abs_pos( '=', 'Y' );
			}
		if	( L6474_GetNbDevices() > 2 )
			{
			L6474_HardStop( 2 );
			edit_abs_pos( '=', 'Z' );
			}
		break;
	default:
		uart2_printf( "cmd '%c'\n", rxchar2 );
	}
}

/* main course */

int main(void)
{
  /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user 
         can eventually implement his proper time base source (a general purpose 
         timer for example or other time source), keeping in mind that Time base 
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and 
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
HAL_Init();

SystemClock_Config();
SystemCoreClockUpdate();

/* Configure leds */
BSP_LED_Init(LED2);
  
uart2_init( 9600 );
gpio_uart2_init();

uart2_printf("C'est imposant !\n");


L6474_SetNbDevices( 2 );

// on remplace L6474_Init() par :
L6474_Board_GpioInit(0);	// un par device	GPIOs pour FLAG (+NVIC), STBY, CS, DIR
L6474_Board_Reset(0);		// reset commun aux 3 devices
L6474_Board_SpiInit();		// SPI commun aux 3 devices
L6474_Board_PwmInit(0);		// un timer par device
soft_param_init(0);		// un par device
if	( L6474_GetNbDevices() > 1 )
	{
	L6474_Board_GpioInit(1);	// un par device
	L6474_Board_PwmInit(1);		// un timer par device
	soft_param_init(1);		// un par device
	}
if	( L6474_GetNbDevices() > 2 )
	{
	L6474_Board_GpioInit(2);	// un par device
	L6474_Board_PwmInit(2);		// un timer par device
	soft_param_init(2);		// un par device
	}
L6474_Board_ReleaseReset(0);	// reset commun aux 3 devices

while	(1)
	{
	if	( rxchar2 )
		{
		serial_command_parser( rxchar2 );
       		rxchar2 = 0;
       		}
	}
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 100000000
  *            HCLK(Hz)                       = 100000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSI Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 400
  *            PLL_P                          = 4
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 3
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if	(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	while(1) {}
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if	(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
	while(1) {}
}
